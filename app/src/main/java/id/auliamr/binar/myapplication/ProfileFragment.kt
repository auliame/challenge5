package id.auliamr.binar.myapplication

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import id.auliamr.binar.myapplication.databinding.FragmentProfileBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.*


class ProfileFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var viewSharedPreferences: SharedPreferences

    private lateinit var user: User
    private lateinit var appDatabase: AppDatabase

    private val c = Calendar.getInstance()
    private val year = c.get(Calendar.YEAR)
    private val month = c.get(Calendar.MONTH)
    private val day = c.get(Calendar.DAY_OF_MONTH)

    private lateinit var id: String
    private lateinit var email: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        (activity as AppCompatActivity?)!!.supportActionBar?.title = "Edit Profile"

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appDatabase = AppDatabase.getInstance(requireContext())!!
        sharedPreferences =
            requireActivity().getSharedPreferences(LoginFragment.PREF_USER, Context.MODE_PRIVATE)
        viewSharedPreferences =
            requireActivity().getSharedPreferences(HomeFragment.PREF_VIEW, Context.MODE_PRIVATE)

        email = sharedPreferences.getString(HomeFragment.EMAIL, "").toString()

        GlobalScope.async {
            user = appDatabase.UserDao().getUserRegistered(email)
            requireActivity().runOnUiThread {
                binding.etUname.setText(user.nama)
                if (user.address != "") {
                    binding.apply {
                        etAlamat.setText(user.address)
                        etNamaLengkap.setText(user.fullName)
                        etTglLahir.setText(user.dateOfBirth)
                    }
                }
            }
        }

        binding.btnUpdate.setOnClickListener(this)
        binding.etTglLahir.setOnClickListener {
            DatePickerDialog(requireContext(), { _, i, i2, i3 ->

                val i3String = i3.toString()
                val dd = if (i3String.length == 1) {
                    "0$i3String"
                } else {
                    i3String
                }
                var mm = ""
                when (i2) {
                    0 -> {
                        mm = "January"
                    }
                    1 -> {
                        mm = "February"
                    }
                    2 -> {
                        mm = "March"
                    }
                    3 -> {
                        mm = "April"
                    }
                    4 -> {
                        mm = "May"
                    }
                    5 -> {
                        mm = "June"
                    }
                    6 -> {
                        mm = "July"
                    }
                    7 -> {
                        mm = "August"
                    }
                    8 -> {
                        mm = "September"
                    }
                    9 -> {
                        mm = "October"
                    }
                    10 -> {
                        mm = "November"
                    }
                    11 -> {
                        mm = "December"
                    }
                }

                val txtDate = "$dd $mm $i"
                binding.etTglLahir.setText(txtDate)
            }, year, month, day).show()
        }
        binding.btnLogout.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_update -> {
                cekInput()
            }
            R.id.btn_logout -> {
                AlertDialog.Builder(requireContext()).setTitle("Logout")
                    .setMessage("Are you sure?")
                    .setIcon(R.mipmap.ic_launcher_round)
                    .setPositiveButton("Yes") { _, _ ->
                        sharedPreferences.edit().clear().apply()
                        viewSharedPreferences.edit().clear().apply()
                        val navOptions =
                            NavOptions.Builder().setPopUpTo(R.id.homeFragment, true).build()
                        Navigation.findNavController(requireView()).navigate(
                            R.id.action_profileFragment_to_loginFragment,
                            null,
                            navOptions
                        )
                    }.setNegativeButton("No") { _, _ ->

                    }
                    .show()
            }
        }
    }

    private fun cekInput() {
        val address: String
        val birthDate: String
        val fullName: String
        val username: String
        binding.apply {
            address = etAlamat.text.toString()
            birthDate = etTglLahir.text.toString()
            fullName = etNamaLengkap.text.toString()
            username = etUname.text.toString()

            if (username.isEmpty()) {
                etUname.error = "Username tidak boleh kosong!"
                return
            }
            if (fullName.isEmpty()) {
                etNamaLengkap.error = "Nama lengkap tidak boleh kosong!"
                return
            }
            if (address.isEmpty()) {
                etAlamat.error = "Alamat tidak boleh kosong!"
                return
            }

            if (birthDate.isEmpty()) {
                etTglLahir.error = "Tanggal lahir can't be empty"
                return
            }
        }
        updateUser()
    }

    private fun updateUser() {
        GlobalScope.async {
            var users = this@ProfileFragment.user
            users = appDatabase.UserDao().getUserRegistered(email)
            val uname = binding.etUname.text.toString()
            val fullName = binding.etNamaLengkap.text.toString()
            val birthDate = binding.etTglLahir.text.toString()
            val address = binding.etAlamat.text.toString()

            val newUser = User(users.email, uname, users.password, fullName, address, birthDate)

            val result = appDatabase.UserDao().updateUser(newUser)
            requireActivity().runOnUiThread {

                if (result != 0) {
                    Toast.makeText(
                        requireView().context,
                        "User berhasil diupdate",
                        Toast.LENGTH_LONG
                    ).show()
                    Navigation.findNavController(requireView())
                        .navigate(R.id.action_profileFragment_to_homeFragment)
                }

            }
        }

    }


}