package id.auliamr.binar.myapplication

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.auliamr.binar.myapplication.databinding.FragmentHomeBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.util.*


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ApiViewModel by activityViewModels()
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var userSharedPreferences: SharedPreferences
    private lateinit var user: User
    private lateinit var appDatabase: AppDatabase


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        sharedPreferences =
            requireActivity().getSharedPreferences(PREF_VIEW, Context.MODE_PRIVATE)
        userSharedPreferences = requireActivity().getSharedPreferences(LoginFragment.PREF_USER, Context.MODE_PRIVATE)

        val ai: ApplicationInfo = requireActivity().applicationContext.packageManager
            .getApplicationInfo(
                requireActivity().applicationContext.packageName,
                PackageManager.GET_META_DATA
            )
        val values = ai.metaData["apiKey"]
        viewModel.apiKey.value = values.toString()

        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appDatabase = AppDatabase.getInstance(requireContext())!!
        sharedPreferences = requireActivity().getSharedPreferences(PREF_USER, Context.MODE_PRIVATE)
        val email = sharedPreferences.getString(EMAIL, "").toString()
        viewModel.getListMovieData()

        val cek = sharedPreferences.getBoolean(CEK, false)
        viewModel.listData.observe(viewLifecycleOwner) {
            if (cek){
                showListLinear(it)
            }else{
                showList(it)
            }

        }

        GlobalScope.async {
            user = appDatabase.UserDao().getUserRegistered(email)
            Log.d("homeFragment", "Data user : $user")
            requireActivity().runOnUiThread {
                val name =
                    user.nama.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
                val txtName = " $name!"

                binding.apply {
                    welcomeUname.append(txtName)
                }
            }

        }

        toProfilePage()

    }

    private fun toProfilePage() {
        binding.btnProfile.setOnClickListener{

            it.findNavController()
                .navigate(R.id.action_homeFragment_to_profileFragment)
        }
    }

    private fun showListLinear(it: List<ResultMovie>?) {
        binding.rvMovie.layoutManager = LinearLayoutManager(requireContext())
        val adapter = MovieAdapter(object : MovieAdapter.OnClickListener {
            override fun onClickItem(data: ResultMovie) {
                viewModel.id.postValue(data.id)
                Navigation.findNavController(requireView())
                    .navigate(R.id.action_homeFragment_to_detailFragment)
            }
        })
        adapter.submitData(it)
        binding.rvMovie.adapter = adapter
    }



    private fun showList(data: List<ResultMovie>?) {
        binding.rvMovie.isNestedScrollingEnabled = false
        binding.rvMovie.layoutManager = GridLayoutManager(requireContext(), 1)
        val adapter = MovieAdapter(object : MovieAdapter.OnClickListener {
            override fun onClickItem(data: ResultMovie) {
                viewModel.id.postValue(data.id)
                Navigation.findNavController(requireView())
                    .navigate(R.id.action_homeFragment_to_detailFragment)
            }
        })
        adapter.submitData(data)
        binding.rvMovie.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.btn_profile -> {
                Navigation.findNavController(requireView())
                    .navigate(R.id.action_homeFragment_to_profileFragment)
                true
            }
            else -> true
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.option_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    companion object{
        const val PREF_VIEW = "view_preference"
        const val CEK = "cek"
        const val PREF_USER = "user_preference"
        const val EMAIL = "email"


    }

}