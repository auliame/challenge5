package id.auliamr.binar.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import id.auliamr.binar.myapplication.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ApiViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)

        (activity as AppCompatActivity?)!!.supportActionBar?.hide()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.id.observe(viewLifecycleOwner) { id ->
            viewModel.getData(id)
        }

        viewModel.data.observe(viewLifecycleOwner) {
            val imgUrl = "https://image.tmdb.org/t/p/w500/${it.posterPath}"
            val date = it.releaseDate.split("-").toTypedArray()
            val genre = it.genres
            var txtGenre = ""
            for ((index, i) in genre.withIndex()) {
                txtGenre += if (index != genre.size - 1) {
                    "${i.name}, "
                } else {
                    i.name
                }
            }
            binding.apply {
                Glide.with(view).load(imgUrl).into(imgMovie)
                tvTitle.text = it.originalTitle
                tvTitle.append(" (${date[0]})")
                tvGenres.text = txtGenre
                tvSypnosis.text = it.overview


            }
        }

    }




}