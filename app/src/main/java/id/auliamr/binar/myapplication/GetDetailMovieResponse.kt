package id.auliamr.binar.myapplication

import com.google.gson.annotations.SerializedName

data class GetDetailMovieResponse(
    @SerializedName("genres")
    val genres: List<Genre>,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("poster_path")
    val posterPath: String
)