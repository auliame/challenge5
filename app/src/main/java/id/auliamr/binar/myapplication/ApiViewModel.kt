package id.auliamr.binar.myapplication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.auliamr.binar.myapplication.GetDetailMovieResponse
import id.auliamr.binar.myapplication.GetPopularMovieResponse
import id.auliamr.binar.myapplication.ResultMovie
import id.auliamr.binar.myapplication.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiViewModel : ViewModel() {
    val apiKey = MutableLiveData<String>()

    val listData = MutableLiveData<List<ResultMovie>>()

    val isSuccess = MutableLiveData<Boolean>()
    val id = MutableLiveData<Int>()
    val failMessage = MutableLiveData<String?>()
    val data = MutableLiveData<GetDetailMovieResponse>()
    val detailFailMessage = MutableLiveData<String?>()

    fun getListMovieData() {
        failMessage.postValue(null)
        ApiClient.instance.getPopularMovie(apiKey.value!!)
            .enqueue(object : Callback<GetPopularMovieResponse> {
                override fun onResponse(
                    call: Call<GetPopularMovieResponse>,
                    response: Response<GetPopularMovieResponse>
                ) {
                    val body = response.body()
                    val code = response.code()
                    if (code == 200){
                        listData.postValue(body?.resultMovies)
                        isSuccess.postValue(true)
                    }else{
                        failMessage.postValue(response.message())
                    }
                }
                override fun onFailure(call: Call<GetPopularMovieResponse>, t: Throwable) {
                    failMessage.postValue(t.message)
                }
            })
    }

    fun getData(id : Int){
        detailFailMessage.postValue(null)
        ApiClient.instance.getDetailMovie(id,apiKey.value!!)
            .enqueue(object : Callback<GetDetailMovieResponse>{
                override fun onResponse(
                    call: Call<GetDetailMovieResponse>,
                    response: Response<GetDetailMovieResponse>
                ) {
                    val code = response.code()
                    if (code == 200){
                        data.postValue(response.body())
                        isSuccess.postValue(true)
                    }
                    else{
                        detailFailMessage.postValue(response.message())
                    }
                }

                override fun onFailure(call: Call<GetDetailMovieResponse>, t: Throwable) {
                    detailFailMessage.postValue(t.message)
                }

            })
    }

}
